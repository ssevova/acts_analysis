import ROOT
import numpy as np
import os
from root_numpy import root2array, tree2array

fs = ['../run/OpenDataDetFatras/fatras-hits.root',
      '../run/GenericDetFatras/fatras-hits.root']

branches = [ 'volume_id','layer_id', 'surface_id',
             'g_x', 'g_y', 'g_z',
             'd_x', 'd_y', 'd_z',
             'value']
selection = ''

for f in fs:
    detname = f.split('/')[2]
    fname = os.path.basename(os.path.normpath(f))
    treename = fname.replace('.root','')
    name = fname.replace('.root','-')
    name += detname
    tree = '{}'.format(treename)

    print('loading {}/{}'.format(f, tree))
    print('branches = {}'.format(branches))
    print('selection = {}'.format(selection))

    arr = root2array(f, tree, branches=branches, selection=selection)

    print('saving {}.npy'.format(name))
    np.save('data_npy/'+name , arr)
