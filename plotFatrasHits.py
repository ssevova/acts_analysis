#!/usr/bin/env python
"""
Plot Fatras-Hits distributions for any det geometry
"""
__author__ = "Stany Sevova"

###############################################################################
# Import libraries
##################
import os
import shutil
import glob
import math

import numpy as np 
import pandas as pd

# Matplotlib
import matplotlib;matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

# plotUtils
from plotUtils import make1Dplot, make2Dplot, makeHTML
###############################################################################
# Load data
###########
odd_hits = np.load('data_npy/fatras-hits-OpenDataDetFatras.npy')
gen_hits = np.load('data_npy/fatras-hits-GenericDetFatras.npy')

###############################################################################
# Make output dir
#################
dir_path = os.path.dirname(os.path.realpath(__file__))
out_dir = "plots_fatras_hits_ODD_Gen"
path = os.path.join(dir_path, out_dir)
if os.path.exists(path):
    shutil.rmtree(path)
os.makedirs(path)
os.chdir(path)

###############################################################################
if __name__ == "__main__":
    ###############################################################################
    # Read in data
    ##################
    
    odd_gx = odd_hits['g_x']
    odd_gy = odd_hits['g_y']
    odd_gz = odd_hits['g_z']
    odd_dx = odd_hits['d_x']
    odd_dy = odd_hits['d_y']
    odd_dz = odd_hits['d_z']
    
    gen_gx = gen_hits['g_x']
    gen_gy = gen_hits['g_y']
    gen_gz = gen_hits['g_z']
    gen_dx = gen_hits['d_x']
    gen_dy = gen_hits['d_y']
    gen_dz = gen_hits['d_z']
    ###############################################################################
    # Plot data
    ##################
    # 1D plots
    make1Dplot(odd_gx,  gen_gx, 'g_x',-1200,1200,False, True)
    make1Dplot(odd_gy,  gen_gy, 'g_y',-1200,1200,False, True)
    make1Dplot(odd_gx,  gen_gz, 'g_z',-2500,2500,False, True)
    make1Dplot(odd_dx,  gen_dx, 'd_x',-1.5,1.5,False, True)
    make1Dplot(odd_dy,  gen_dy, 'd_y',-1.5,1.5,False, True)
    make1Dplot(odd_dx,  gen_dz, 'd_z',-1.5,1.5,False, True)
    
    # 2D plots
    make2Dplot(odd_gx, odd_gy, 'ODD_g_x','ODD_g_y')
    make2Dplot(gen_gx, gen_gy, 'gen_g_x','gen_g_y')

    ###############################################################################
    # Make a web-page
    ##################
    makeHTML("plots.html","ODD vs generic hits")
