import ROOT
import numpy as np
import os
from root_numpy import root2array, tree2array

fs = ['../run/GenericDetProp/propagation-steps.root',
      '../run/OpenDataDetProp/propagation-steps.root']

branches = [ 'event_nr',
             'volume_id','layer_id', 'boundary_id', 'approach_id', 'sensitive_id',
             'g_x', 'g_y', 'g_z',
             'd_x', 'd_y', 'd_z',
             'type', 'step_acc', 'step_act', 'step_abt', 'step_usr']
selection = ''

for f in fs:
    detname = f.split('/')[2]
    fname = os.path.basename(os.path.normpath(f))
    treename = fname.replace('.root','')
    treename = treename.replace('-','_')
    name = fname.replace('.root','-')
    name += detname
    tree = '{}'.format(treename)

    print('loading {}/{}'.format(f, tree))
    print('branches = {}'.format(branches))
    print('selection = {}'.format(selection))
    
    arr = root2array(f, tree, branches=branches, selection=selection)

    print('saving {}.npy'.format(name))
    np.save(name , arr)
