import ROOT
import numpy as np
import os
from root_numpy import root2array, tree2array

fs = ['../run/OpenDataDetFatrasParticleGun/fitted-tracks.root',
      '../run/GenericDetFatrasParticleGun/fitted-tracks.root']

branches = [ 'event_nr','t_vx', 't_vy', 't_vz', 
             't_px', 't_py', 't_pz', 
             't_theta', 't_phi', 't_eta','t_pT',
             'g_x_hit','g_y_hit','g_z_hit']

selection = ''

for f in fs:

    detname = f.split('/')[2]
    fname = os.path.basename(os.path.normpath(f))

    treename = fname.replace('.root','')
    name = fname.replace('.root','-gun-')
    name += detname
    tree = '{}'.format(treename)

    print('loading {}/{}'.format(f, tree))
    print('branches = {}'.format(branches))
    print('selection = {}'.format(selection))

    arr = root2array(f, tree, branches=branches, selection=selection)
    
    print('saving {}.npy'.format(name))
    np.save(name , arr)
