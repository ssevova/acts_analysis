#!/usr/bin/env python
"""
Plot Fatras fitted tracks & other distributions for any det geometry
"""
__author__ = "Stany Sevova"

###############################################################################
# Import libraries
##################
import os
import shutil
import glob
import math

import numpy as np 
import numpy.lib.recfunctions as recfn

import pandas as pd

# Matplotlib
import matplotlib;matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

# plotUtils
from plotUtils import make1Dplot, make2Dplot, makeHTML

###############################################################################
# Load data
###########
odd_tracks = np.load('data_npy/fitted-tracks-OpenDataDetFatras.npy')
gen_tracks = np.load('data_npy/fitted-tracks-GenericDetFatras.npy')

###############################################################################
# Make output dir
#################
dir_path = os.path.dirname(os.path.realpath(__file__))
out_dir = "plots_fitted_tracks_ODD_Gen"
path = os.path.join(dir_path, out_dir)
if os.path.exists(path):
    shutil.rmtree(path)
os.makedirs(path)
os.chdir(path)
###############################################################################
if __name__ == "__main__":
    ###############################################################################
    # Read in data
    ##################
    
    odd_px = odd_tracks['t_px']
    odd_py = odd_tracks['t_py']
    odd_pz = odd_tracks['t_pz']
    odd_theta = odd_tracks['t_theta']
    odd_eta = odd_tracks['t_eta']
    odd_phi = odd_tracks['t_phi']
    odd_pt  = odd_tracks['t_pT']
    odd_vx  = odd_tracks['t_vx']
    odd_vy  = odd_tracks['t_vy']
    odd_vz  = odd_tracks['t_vz']
    odd_gx  = odd_tracks['g_x_hit']
    odd_gy  = odd_tracks['g_y_hit']
    odd_gz  = odd_tracks['g_z_hit']
    
    gen_px = gen_tracks['t_px']
    gen_py = gen_tracks['t_py']
    gen_pz = gen_tracks['t_pz']
    gen_theta = gen_tracks['t_theta']
    gen_eta = gen_tracks['t_eta']
    gen_phi = gen_tracks['t_phi']
    gen_pt  = gen_tracks['t_pT']
    gen_vx  = gen_tracks['t_vx']
    gen_vy  = gen_tracks['t_vy']
    gen_vz  = gen_tracks['t_vz']
    gen_gx  = gen_tracks['g_x_hit']
    gen_gy  = gen_tracks['g_y_hit']
    gen_gz  = gen_tracks['g_z_hit']
 
    ###############################################################################
    # Plot data
    ##################
    # 1D plots
    make1Dplot(odd_pt,  gen_pt, 'pt',0,7,True, True)
    make1Dplot(odd_pt,  gen_pt, 'pt',0,7,False, True)
    make1Dplot(odd_px,  gen_px, 'px',0,7,False, True)
    make1Dplot(odd_py,  gen_py, 'py',0,7,False, True)
    make1Dplot(odd_pz,  gen_pz, 'pz',0,7,False, True)
    make1Dplot(odd_eta, gen_eta,'eta',-6,6,False, True)
    make1Dplot(odd_phi, gen_phi,'phi',np.amin(odd_phi),np.amax(odd_phi),False, True)
    make1Dplot(odd_gx,  gen_gx, 'g_x_hit',-1200,1200,False, True)
    make1Dplot(odd_gy,  gen_gy, 'g_y_hit',-1200,1200,False, True)
    make1Dplot(odd_gx,  gen_gz, 'g_z_hit',-2500,2500,False, True)
    make1Dplot(odd_vx,  gen_vx, 'v_x',-350,350,False, True)
    make1Dplot(odd_vy,  gen_vy, 'v_y',-350,350,False, True)
    make1Dplot(odd_vx,  gen_vz, 'v_z',-250,250,False, True)
    
    
    # 2D plots
    make2Dplot(odd_gx, odd_gy, 'ODD_g_x','ODD_g_y')
    make2Dplot(gen_gx, gen_gy, 'gen_g_x','gen_g_y')

    # Make web-page
    makeHTML("plots.html","ODD vs generic plots")
