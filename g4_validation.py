#!/usr/bin/env python
"""
Plot the energy loss of a particle traversing given detector geometry
"""
__author__ = "Stanislava Sevova"

###############################################################################                                   
# Import libraries                                                                                                
##################  
import time
import uproot as up
import numpy as np
import os
import glob
import shutil
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from plotUtils import getRatio, makeHTML

###############################################################################                                   
# Load data                                                                                                       
###########       
g4fname="../data/geant-track-steps-mu%sGeV-nevts10000-detODD.root"
muEnergy = ["100"]
samCols = ['b', 'k', 'r']

###############################################################################                                   
# Make output dir                                                                                                 
#################                                                                                                 
dir_path = os.path.dirname(os.path.realpath(__file__))
out_dir = "plots_g4_energy_loss_mu%s"%muEnergy[0]
path = os.path.join(dir_path, out_dir)
if os.path.exists(path):
    shutil.rmtree(path)
os.makedirs(path)
os.chdir(path)

###############################################################################                                   
if __name__ == "__main__":
    ###############################################################################
    # Read in data                                                                                                 
    ##################      

    # histELoss = []
    # histdEdx  = []
    # x_ELoss   = []
    # y_Length  = []
    

    
    e_array = []
    #radii = np.array([0, 40, 80, 120, 200, 300, 400, 500, 600, 800, 1200])
    radii = np.array([0, 35, 75, 120, 175, 275, 375, 510, 670, 850, 1050, 1300])
    
    rmin = np.array([28, 56, 100, 160, 240, 340, 480, 640, 790, 990])
    rmax = np.array([44, 82, 126, 182, 280, 380, 520, 680, 840, 1040])

    isam      = 0 # sample iterator
    ###############################################################################                               
    # Loop over all the geant files
    ###############################################################################
    for path, file, start, stop, entry in up.iterate([g4fname % x
                                                      for x in muEnergy],
                                                     "steps",
                                                     branches=[
                                                         "event_id",
                                                         "tx",
                                                         "ty",
                                                         "tz",
                                                         "tpx",
                                                         "tpy",
                                                         "tpz",
                                                         "te"
                                                     ],
                                                     namedecode="utf-8",
                                                     reportpath=True,
                                                     reportfile=True,
                                                     reportentries=True):
        
        print('==> Processing sample: %s ...'%path)
        # Declare some histograms
        hELoss  = None
        hdEdx   = None
        xELoss  = None
        yLength = None
        
        # Read in the tree
        g4tree = up.open(path)["steps"]
        
        # Make the particle dataframe: event id, position, energy, momentum
        particle_df=g4tree.pandas.df(["event_id","t[xyz]","te","tp[xyz]"])
        
        #===========================================================================
        # Calculate/compute variables
        #===========================================================================
        # Calculate radial position
        particle_df['tr'] = ( particle_df['tx']*particle_df['tx'] +
                              particle_df['ty']*particle_df['ty'] ) ** 0.5
        
        # Convert energy, px, py, pz to GeV
        particle_df['te']  = particle_df['te'].truediv(1000)
        particle_df['tpx'] = particle_df['tpx'].truediv(1000)
        particle_df['tpy'] = particle_df['tpy'].truediv(1000)
        particle_df['tpz'] = particle_df['tpz'].truediv(1000)
        
        # Calculate momentum 
        particle_df['tp'] = (particle_df['tpx']*particle_df['tpx'] +
                             particle_df['tpy']*particle_df['tpy'] +
                             particle_df['tpz']*particle_df['tpz']) ** 0.5
        
        # Calculate pseudorapidity
        particle_df['teta'] = 0.5 * np.log( (particle_df['tp'] + particle_df['tpz'])/
                                            (particle_df['tp'] - particle_df['tpz']) )
        
        #===========================================================================
        # Selection
        #===========================================================================
        # Barrel: |eta| < 1.0
        particle_df = particle_df[particle_df['teta'].abs() < 1.0]
        
        #===========================================================================
        # Fill histograms
        #===========================================================================
        # Bin the energies by radial distance 
        energy = []
        for i in range(len(radii)):
            if i == len(radii)-1:
                arr = np.array(particle_df[(particle_df['tr'] > radii[i])]['te'])
            else:
                arr = np.array(particle_df[(particle_df['tr'] > radii[i]) &
                                           (particle_df['tr'] <= radii[i+1])]['te'])
                energy.append(np.divide(np.sum(arr),len(arr)))
                e_array.append(energy)
                
        # Calculate energy loss from rmin to rmax for different boundary conditions
        for i in range(len(rmin)):
            par_eloss_in  = []
            par_eloss_out = []
            par_eloss_closest = []
            inside_df = particle_df[(particle_df['tr'] > rmin[i]) & (particle_df['tr'] < rmax[i])]
            outside_df= particle_df[(particle_df['tr'] < rmin[i]) | (particle_df['tr'] > rmax[i])]

            #1. Closest to rmin, rmax 
            for idx, new_df in particle_df.groupby('event_id'):
                closest_rmin_df = new_df.iloc[(new_df['tr']-rmin[i]).abs().argsort()[:1]]
                closest_rmax_df = new_df.iloc[(new_df['tr']-rmax[i]).abs().argsort()[:1]]
                eloss  = closest_rmin_df['te'].values[0] - closest_rmax_df['te'].values[0]
                par_eloss_closest.append(eloss)
                
            #2. Closest to outer boundary of rmin & rmax
            for idx, new_df in outside_df.groupby('event_id'):
                found_min = new_df.iloc[(new_df['tr']-rmin[i]).abs().argsort()[:1]]
                rmin_idx  = found_min.index[0] # get the index of the rmin position
                new_df    = new_df.drop([rmin_idx]) # remove the rmin row from the df
                if new_df.empty: continue

                found_max = new_df.iloc[(new_df['tr']-rmax[i]).abs().argsort()[:1]] #find closest to max
                eloss     = found_min['te'].values[0] - found_max['te'].values[0]
                par_eloss_out.append(eloss)
            
            #3. Closest to inner boundary of rmin & rmax
            for idx, new_df in inside_df.groupby('event_id'):
                eloss=(new_df['te'].iloc[0] - new_df['te'].iloc[-1])
                par_eloss_in.append(eloss)

            # Plots!
            fig, ax = plt.subplots(2,1)
            c_in, edges_in       = np.histogram(par_eloss_in, bins=50, range=(0.00005,0.00300))
            c_out, edges_out     = np.histogram(par_eloss_out, bins=50, range=(0.00005,0.00300))
            c_close, edges_close = np.histogram(par_eloss_closest, bins=50, range=(0.00005,0.00300))
            ax[0].errorbar(x=edges_in,y=np.append(c_in,0), yerr=np.sqrt(np.append(c_in,0)),
                           label=('mu %s GeV (in)' %muEnergy[isam]),color=samCols[isam])
            ax[0].errorbar(x=edges_out,y=np.append(c_out,0), yerr=np.sqrt(np.append(c_out,0)),
                           label=('mu %s GeV (out)' %muEnergy[isam]),color=samCols[isam+1])
            ax[0].errorbar(x=edges_close,y=np.append(c_close,0), yerr=np.sqrt(np.append(c_close,0)),
                           label=('mu %s GeV (closest)' %muEnergy[isam]),color=samCols[isam+2])
            ax[0].set_ylabel("Number of particles")
            ax[0].legend()
            
            ratio  = getRatio(np.append(c_in,0),np.append(c_out,0))
            ax[1].plot(edges_in,ratio)
            ax[1].set_ylim(0,2)
            ax[1].set_xlabel("Energy loss [GeV]")
            ax[1].set_ylabel("in/out")
            plt.savefig("particle_energy_loss_rmin%i_rmax%i_mu%sGeV.pdf"%(rmin[i],rmax[i],muEnergy[isam]),format="pdf")


        # Make website
        makeHTML("energy_loss_plots.html","G4 energy loss")
    
        # #===========================================================================
        # # Loop over file entries
        # #===========================================================================
        # for i in range(g4tree.numentries):
            
        #     totELoss  = np.divide(np.sum(entry["mat_tot_eloss"][i]),1000)
        #     totLength = np.sum(entry["mat_step_length"][i])
        #     dEdx      = np.divide(totELoss,totLength)
            
        #     cELoss, eELoss= np.histogram(totELoss, bins=100, range=(0,0.2))
        #     cdEdx, edEdx   = np.histogram(dEdx, bins=100, range=(0, 4e-4))
        #     if hELoss is None:
        #         hELoss = cELoss, eELoss
        #         hdEdx  = cdEdx, edEdx
        #         xELoss = totELoss
        #         yLength = totLength
        #     else:
        #         hELoss  = hELoss[0] + cELoss, eELoss
        #         hdEdx   = hdEdx[0] + cdEdx, edEdx
        #         xELoss  = np.append(xELoss,totELoss)
        #         yLength = np.append(yLength,totLength)
                
        #         histELoss.append(hELoss)
        #         histdEdx.append(hdEdx)
        #         x_ELoss.append(xELoss)
        #         y_Length.append(yLength)
        #         isam += 1

        # #===========================================================================
        # # Plot histograms
        # #===========================================================================
        # #Plot 1D histos
        # fig, ax = plt.subplots(1,2)
        # for i in range(isam):
        #     c, e = histELoss[i]
        #     ax[0].step(x=e, y=np.append(c,0), label=('mu %s GeV' %muEnergy[i]), color=samCols[i], where="post")
        #     ax[0].set_xlim(e[0], e[-1])
        #     ax[0].set_ylim(0, c.max() * 1.15)
        #     ax[0].set_xlabel("Energy loss [GeV]")
        #     ax[0].set_ylabel("Number of particles")
        #     ax[0].legend()
            
        # for i in range(isam):
        #     c, e = histdEdx[i]
        #     ax[1].step(x=e, y=np.append(c,0), label=('mu %s GeV' %muEnergy[i]), color=samCols[i], where="post")
        #     ax[1].set_xlim(e[0], e[-1])
        #     ax[1].set_ylim(0, c.max() * 1.15)
        #     ax[1].set_xlabel("dE/dx [GeV/mm]")
        #     ax[1].set_ylabel("Number of particles")
        #     ax[1].legend()
        #     fig.tight_layout()
        #     plt.savefig("g4energyloss_pi.pdf",format="pdf")

        # #Plot individual histos
        # for i in range(isam):
        #     fig, ax = plt.subplots()
        #     h=ax.hist2d(x_ELoss[i],y_Length[i],100,norm=mcolors.LogNorm())
        #     ax.grid(True)
        #     ax.set_xlabel("Energy loss [GeV]")
        #     ax.set_ylabel("Total length [mm]")
        #     fig.colorbar(h[3],ax=ax)
        #     plt.savefig('eloss_vs_length_mu%sGeV.pdf'%muEnergy[i], format="pdf")
            
        #     fig, ax = plt.subplots()
        #     bincenters = 0.5*(radii[1:]+radii[:-1])
        #     bincenters = np.append(bincenters,radii[-1]+200)
        #     x_err      = 0.5*(radii[:-1]-radii[1:])
        #     x_err      = np.append(x_err,200)
        #     energy = e_array[i]
        #     ax.errorbar(x=bincenters, y=energy, xerr=x_err, label=('mu %s GeV'  %muEnergy[i]), color=samCols[i])
        #     ax.set_xlabel('radial distance [mm]')
        #     ax.set_ylabel('<E> [GeV]')
        #     ax.set_ylim(bottom=int(muEnergy[i])+0.05,top=int(muEnergy[i])+0.13)
        #     ax.set_xlim(left=0)
        #     ax.minorticks_on()
        #     ax.grid(True)
        #     ax.legend()
        #     plt.savefig("avg_energy_vs_rad_mu%sGeV.pdf"%muEnergy[i],format="pdf")
